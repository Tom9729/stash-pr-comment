# Stash Pull Request Comment Script

## Description
Quick script to let Jenkins post comments on an open pull request
in Stash after building a branch.

## Usage

    $ echo "Message" | ./stash-pr-comment.py <project> <repo> <branch>

## Install

1. Edit the script to update the URL/USER/PASS variables at the top and then save somewhere safe that Jenkins can execute. The credentials must be for a licensed Stash user that can comment on pull requests in your project.

2. Make sure that you have a recent version of Python 2 and `python-requests` is installed.

3. Update your Jenkins job to call the script after your build.

## Testing

Run the test server (needs netcat installed)

    $ ./test-server.sh

Then run the comment poster (arguments: project, repo, branch)

    $ echo "hello world" | ./stash-pr-comment.py FOO foo refs/heads/feature-ABC-123
    Found 1 pull requests at http://localhost:8080/stash/rest/api/1.0/projects/FOO/repos/foo
    Found PR for branch refs/heads/feature-ABC-123
    Posting comment to PR #101
    Done

Test server output should look something like

    $ ./test-server.sh 
    waiting for connection
    
    GET /stash/rest/api/1.0/projects/FOO/repos/foo/pull-requests HTTP/1.1
    Host: localhost:8080
    Authorization: Basic amVua2luczpjaGFuZ2VtZQ==
    Accept-Encoding: gzip, deflate, compress
    Accept: */*
    User-Agent: python-requests/2.2.1 CPython/2.7.6 Linux/3.13.0-74-generic
    
    POST /stash/rest/api/1.0/projects/FOO/repos/foo/pull-requests/101/comments HTTP/1.1
    Host: localhost:8080
    Content-Length: 23
    Authorization: Basic amVua2luczpjaGFuZ2VtZQ==
    Accept-Encoding: gzip, deflate, compress
    Accept: */*
    User-Agent: python-requests/2.2.1 CPython/2.7.6 Linux/3.13.0-74-generic
    
    {"text": "hello world"}
    done

## Links

- [Stash REST API](https://developer.atlassian.com/static/rest/stash/3.8.0/stash-rest.html)
- [Python Requests](http://docs.python-requests.org/en/latest/user/quickstart/)
