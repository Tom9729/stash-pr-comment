#!/usr/bin/env python

"""
Stash Pull Request Comment Script 
Version 1.0
https://bitbucket.org/Tom9729/stash-pr-comment
"""

STASH_URL = "http://localhost:8080/stash"
STASH_USER = "jenkins"
STASH_PASS = "changeme"

## CONFIG ENDS HERE

import sys
import requests
import json
import time

if not len(sys.argv) == 4:
    sys.stderr.write("usage: " + sys.argv[0] + " <project> <repo> <branch>\n")
    sys.exit(1)

project = sys.argv[1]
repo = sys.argv[2]
url = STASH_URL + "/rest/api/1.0/projects/" + project + "/repos/" + repo
branch = sys.argv[3]
message = {
    "text": ''.join(sys.stdin.readlines())
}
auth = (STASH_USER, STASH_PASS)

## Get list of PRs
prList = requests.get(url + "/pull-requests",
                      auth=auth).json() 
print "Found " + str(len(prList["values"])) + " pull requests at " + url
time.sleep(0.1)

## PR for our branch.
prForBranch = None

## Look for PR where FROM branch = desired
for pr in prList["values"]:
    if pr["fromRef"]["id"] == branch:
        print "Found PR for branch " + branch
        prForBranch = pr
        break

## If not found then exit
if prForBranch == None:
    print "Couldn't find PR for branch " + branch
    sys.exit(0)

## Post comment to PR
prId = str(prForBranch["id"])
print "Posting comment to PR #" + prId
requests.post(url + "/pull-requests/" + prId + "/comments",
              auth=auth,
              data=json.dumps(message))

print "Done"
